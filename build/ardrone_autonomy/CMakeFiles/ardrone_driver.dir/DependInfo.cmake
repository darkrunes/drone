# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/rsa/rsa_drone/src/ardrone_autonomy/src/ardrone_driver.cpp" "/home/rsa/rsa_drone/build/ardrone_autonomy/CMakeFiles/ardrone_driver.dir/src/ardrone_driver.cpp.o"
  "/home/rsa/rsa_drone/src/ardrone_autonomy/src/ardrone_sdk.cpp" "/home/rsa/rsa_drone/build/ardrone_autonomy/CMakeFiles/ardrone_driver.dir/src/ardrone_sdk.cpp.o"
  "/home/rsa/rsa_drone/src/ardrone_autonomy/src/teleop_twist.cpp" "/home/rsa/rsa_drone/build/ardrone_autonomy/CMakeFiles/ardrone_driver.dir/src/teleop_twist.cpp.o"
  "/home/rsa/rsa_drone/src/ardrone_autonomy/src/video.cpp" "/home/rsa/rsa_drone/build/ardrone_autonomy/CMakeFiles/ardrone_driver.dir/src/video.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "ROSCONSOLE_BACKEND_LOG4CXX"
  "ROS_BUILD_SHARED_LIBS=1"
  "ROS_PACKAGE_NAME=\"ardrone_autonomy\""
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "/home/rsa/rsa_drone/devel/include"
  "/home/rsa/rsa_drone/devel/src/ardronelib/ARDroneLib"
  "/home/rsa/rsa_drone/devel/src/ardronelib/ARDroneLib/FFMPEG/Includes"
  "/home/rsa/rsa_drone/devel/src/ardronelib/ARDroneLib/Soft/Common"
  "/home/rsa/rsa_drone/devel/src/ardronelib/ARDroneLib/Soft/Lib"
  "/home/rsa/rsa_drone/devel/src/ardronelib/ARDroneLib/VP_SDK"
  "/home/rsa/rsa_drone/devel/src/ardronelib/ARDroneLib/VP_SDK/VP_Os/linux"
  "/opt/ros/indigo/include"
  "/home/rsa/rsa_drone/src/ardrone_autonomy/include"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
