#!/usr/bin/env python
'''
HSV range constants for colour detection with fancy names
Ben Soh
'''

# magenta
# MAGENTA_LOWER = [153, 82, 223]
# MAGENTA_UPPER = [168, 197, 255]
MAGENTA_LOWER = [142, 61, 154]
MAGENTA_UPPER = [172, 240, 255]

# green
LIME_LOWER = [0, 65, 81]
LIME_UPPER = [91, 255, 255]

# blue
MIDNIGHTBLUE_LOWER = [103, 129, 124]
MIDNIGHTBLUE_UPPER = [146, 255, 235]
